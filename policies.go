package appdynamics

import "fmt"

// Get policies from AppDynamics
func (a AppDynamics) GetPolicies(applicationID int) ([]byte, int, error) {
	return a.getJSON(fmt.Sprintf("%scontroller/policies/%d", a.url, applicationID))
}

// post policies to AppDynamics
func (a AppDynamics) PostPolicies(uploadFile string, applicationID int, overwrite bool) (string, int, error) {
	return a.postJSON(fmt.Sprintf("%scontroller/policies/%d?overwrite=%t", a.url, applicationID, overwrite), uploadFile)
}