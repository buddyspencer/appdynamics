package appdynamics

import "fmt"

// Get actions from AppDynamics
func (a AppDynamics) GetActions(applicationID int) ([]byte, int, error) {
	return a.getJSON(fmt.Sprintf("%scontroller/actions/%d", a.url, applicationID))
}

// post actions to AppDynamics
func (a AppDynamics) PostActions(uploadFile string, applicationID int) (string, int, error) {
	return a.postJSON(fmt.Sprintf("%scontroller/actions/%d", a.url, applicationID), uploadFile)
}

