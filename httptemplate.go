package appdynamics

import "fmt"

// Get http-template from AppDynamics
func (a AppDynamics) GetHttpTemplates() ([]byte, int, error) {
	return a.getJSON(fmt.Sprintf("%scontroller/actiontemplate/httprequest", a.url))
}

// post http-template to AppDynamics
func (a AppDynamics) PostHttpTemplates(uploadFile string) (string, int, error) {
	return a.postJSON(fmt.Sprintf("%scontroller/actiontemplate/httprequest", a.url), uploadFile)
}