package appdynamics

import "net/http"

// AppDynamics Struct
type AppDynamics struct {
	account  string
	username string
	password string
	url      string
	client   http.Client
}

// Applications Struct
type Applications struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	AccountGuid string `json:"accountGuid"`
}

// Businesstransactions Struct
type BusinessTransactions struct {
	InternalName         string `json:"internalName"`
	TierID               int    `json:"tierId"`
	EntryPointType       string `json:"entryPointType"`
	Background           bool   `json:"background"`
	TierName             string `json:"tierName"`
	Name                 string `json:"name"`
	ID                   int    `json:"id"`
	EntryPointTypeString string `json:"entryPointTypeString"`
}
