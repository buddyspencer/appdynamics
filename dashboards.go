package appdynamics

import (
	"fmt"
)

// Get dashboards from AppDynamics
func (a AppDynamics) GetDashboard(dashboardID int) ([]byte, int, error) {
	return a.getJSON(fmt.Sprintf("%scontroller/CustomDashboardImportExportServlet?dashboardId=%d", a.url, dashboardID))
}

// post dashboards to AppDynamics
func (a AppDynamics) PostDashboard(uploadFile string) (string, int, error) {
	return a.postJSON(fmt.Sprintf("%scontroller/CustomDashboardImportExportServlet", a.url), uploadFile)
}
