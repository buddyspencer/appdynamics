package appdynamics

import (
	"encoding/json"
	"fmt"
)

// Get applications in XML format from AppDynamics
func (a AppDynamics) GetApplications() ([]Applications, int, error) {
	content, statuscode, err := a.getJSON(fmt.Sprintf("%scontroller/rest/applications?output=JSON", a.url))
	if err != nil {
		return []Applications{}, statuscode, err
	}
	if statuscode == 200 {
		var applications []Applications
		err := json.Unmarshal(content, &applications)
		return applications, statuscode, err
	}
	return []Applications{}, statuscode, err
}

// Get business-transactions in XML format from AppDynamics
func (a AppDynamics) GetBusinessTransactions(applicationID interface{}) ([]BusinessTransactions, int, error) {
	content, statuscode, err := a.getJSON(fmt.Sprintf("%s/controller/rest/applications/%v/business-transactions?output=JSON", a.url, applicationID))
	if err != nil {
		return []BusinessTransactions{}, statuscode, err
	}
	if statuscode == 200 {
		var businesstransactions []BusinessTransactions
		err := json.Unmarshal(content, &businesstransactions)
		return businesstransactions, statuscode, err
	}
	return []BusinessTransactions{}, statuscode, err
}
