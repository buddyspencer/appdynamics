package appdynamics

import (
	"net/http"
	"strings"
)

// Initialize AppDynamics
func Init(account, username, password, url string) AppDynamics {
	if ! strings.HasSuffix(url, "/") {
		url += "/"
	}
	return AppDynamics{account, username, password, url, http.Client{}}
}
