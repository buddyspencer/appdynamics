package appdynamics

import (
	"fmt"
	"net/url"

	"github.com/clbanning/mxj"
)

// Get health-rules from AppDynamics
func (a AppDynamics) GetHealthRules(applicationID int) (mxj.Map, int, error) {
	return a.getXML(fmt.Sprintf("%scontroller/healthrules/%d", a.url, applicationID))
}

// Get health-rules from AppDynamics in JSON Format
func (a AppDynamics) GetHealthRulesJSON(applicationID int) ([]byte, int, error) {
	return a.getJSON(fmt.Sprintf("%scontroller/alerting/rest/v1/applications/%d/health-rules", a.url, applicationID))
}

// Get a specific health-rule from AppDynamics
func (a AppDynamics) GetHealthRule(applicationID int, healthRule string) (mxj.Map, int, error) {
	return a.getXML(fmt.Sprintf("%scontroller/healthrules/%d?name=%s", a.url, applicationID, url.QueryEscape(healthRule)))
}

// Get health-rule from AppDynamics in JSON Format
func (a AppDynamics) GetHealthRuleJSON(applicationID, healthRuleID int) ([]byte, int, error) {
	return a.getJSON(fmt.Sprintf("%scontroller/alerting/rest/v1/applications/%d/health-rules/%d", a.url, applicationID, healthRuleID))
}

// post health-rules to AppDynamics
func (a AppDynamics) PostHealthRules(applicationID int, uploadFile string, overwrite bool) (string, int, error) {
	return a.postXML(fmt.Sprintf("%scontroller/healthrules/%d?overwrite=%t", a.url, applicationID, overwrite), uploadFile)
}

// post health-rules to AppDynamics in JSON Format
func (a AppDynamics) PostHealthRulesJSON(applicationID int, uploadFile string) (string, int, error) {
	return a.postJSON(fmt.Sprintf("%scontroller/alerting/rest/v1/applications/%d/health-rules", a.url, applicationID), uploadFile)
}

// put health-rules to AppDynamics in JSON Format
func (a AppDynamics) PutHealthRulesJSON(applicationID, healthRuleID int, uploadFile string) (string, int, error) {
	return a.putJSON(fmt.Sprintf("%scontroller/alerting/rest/v1/applications/%d/health-rules/%d/configuration", a.url, applicationID, healthRuleID), uploadFile)
}
