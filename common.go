package appdynamics

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"github.com/clbanning/mxj"
)

// Get XML from AppDynamics
func (a AppDynamics) getXML(request string) (mxj.Map, int, error) {
	req, err := http.NewRequest("GET", request, nil)
	if err != nil {
		return mxj.New(), 0, err
	}
	req.SetBasicAuth(fmt.Sprintf("%s@%s", a.account, a.username), a.password)
	resp, err := a.client.Do(req)
	if err != nil {
		return mxj.New(), 0, err
	}
	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	mv, err := mxj.NewMapXml(content)
	return mv, resp.StatusCode, err
}

// Post XML to AppDynamics
func (a AppDynamics) postXML(request string, uploadFile string) (string, int, error) {
	file, _ := os.Open(uploadFile)
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	_, err := io.Copy(part, file)
	if err != nil {
		return "", 0, err
	}
	writer.Close()

	req, err := http.NewRequest("POST", request, body)
	if err != nil {
		return "", 0, err
	}
	req.SetBasicAuth(fmt.Sprintf("%s@%s", a.account, a.username), a.password)
	req.Header.Add("Content-type", writer.FormDataContentType())
	resp, err := a.client.Do(req)
	if err != nil {
		return "", 0, err
	}
	content, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return string(content), resp.StatusCode, err
}

// Get JSON from AppDynamics
func (a AppDynamics) getJSON(request string) ([]byte, int, error) {
	req, err := http.NewRequest("GET", request, nil)
	if err != nil {
		return []byte{}, 0, err
	}
	req.SetBasicAuth(fmt.Sprintf("%s@%s", a.account, a.username), a.password)
	resp, err := a.client.Do(req)
	if err != nil {
		return []byte{}, 0, err
	}
	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	return content, resp.StatusCode, err
}

// Post JSON to AppDynamics
func (a AppDynamics) postJSON(request string, uploadFile string) (string, int, error) {
	file, _ := os.Open(uploadFile)
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	_, err := io.Copy(part, file)
	if err != nil {
		return "", 0, err
	}
	writer.Close()

	req, err := http.NewRequest("POST", request, body)
	if err != nil {
		return "", 0, err
	}
	req.SetBasicAuth(fmt.Sprintf("%s@%s", a.account, a.username), a.password)
	req.Header.Add("Content-type", "application/json")
	resp, err := a.client.Do(req)
	if err != nil {
		return "", 0, err
	}
	content, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return string(content), resp.StatusCode, err
}

// Put JSON to AppDynamics
func (a AppDynamics) putJSON(request string, uploadFile string) (string, int, error) {
	file, _ := os.Open(uploadFile)
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	_, err := io.Copy(part, file)
	if err != nil {
		return "", 0, err
	}
	writer.Close()

	req, err := http.NewRequest("PUT", request, body)
	if err != nil {
		return "", 0, err
	}
	req.SetBasicAuth(fmt.Sprintf("%s@%s", a.account, a.username), a.password)
	req.Header.Add("Content-type", writer.FormDataContentType())
	resp, err := a.client.Do(req)
	if err != nil {
		return "", 0, err
	}
	content, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return string(content), resp.StatusCode, err
}

// Set an httpclient
func (a AppDynamics) SetHttpClient(client http.Client) {
	a.client = client
}
