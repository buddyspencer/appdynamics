package appdynamics

import "fmt"

// Get email-template from AppDynamics
func (a AppDynamics) GetEmailTemplates() ([]byte, int, error) {
	return a.getJSON(fmt.Sprintf("%scontroller/actiontemplate/email", a.url))
}

// post email-template to AppDynamics
func (a AppDynamics) PostEmailTemplates(uploadFile string) (string, int, error) {
	return a.postJSON(fmt.Sprintf("%scontroller/actiontemplate/email", a.url), uploadFile)
}